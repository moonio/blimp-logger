'use strict';

// Module dependencies.
var fs = require('fs'),
  path = require('path'),
  _ = require('lodash');

// Initialize Logger
function Logger() {

}

// Logger methods
Logger.prototype = {

  /**
   * Logger Model Initialization
   *
   * @return {Logger}
   */
  initModel: function() {
    var self = this;

    if (self.mongoose) {
      var logSchema = new self.mongoose.Schema({
        message: {
          type: String,
          trim: true,
          required: true
        },
        category: {
          type: String,
          trim: true
        },
        created: {
          type: Date,
          default: Date.now
        },
      });

      self.mongoose.model('Log', logSchema);
    }

    return self;
  },

  /**
   * Logger Routes Initialization
   *
   * @return {Logger}
   */
  initRoutes: function() {
    var self = this;

    self.app.post('/logs', function(req, res) {
      var log = new self.mongoose.models.Log();
      log.message = req.query.message;
      if (req.query.category) {
        log.category = req.query.category;
      }
      log.save();

      res.jsonp(log);
    });

    self.app.get('/logs', function(req, res) {
      self.mongoose.models.Log.find().sort('-created').exec(function(err, logs) {
        if (err) {
          res.render('error', {
            status: 500
          });
        } else {
          res.jsonp(logs);
        }
      });
    });

    self.app.get('/logs/:category', function(req, res) {
      var cat = req.params.category;
      self.mongoose.models.Log.find({category: cat}).sort('-created').exec(function(err, logs) {
        if (err) {
          res.render('error', {
            status: 500
          });
        } else {
          res.jsonp(logs);
        }
      });
    });

    return self;
  },

  /**
   * Initializng
   *
   * @param {Express} app
   * @param {Mongoose} mongoose
   * @return {Logger}
   */
  initialize: function(app, mongoose, opts) {
    var self = this,
      options;

    options = _.extend({
      console: false
    }, opts);

    //Checking for valid init
    if (!app || !mongoose) {
      throw new Error('Error initializing Logger [missing params]!');
    }

    //Setting app global variables
    self.app = app;
    self.mongoose = mongoose;
    self.options = options;

    //Initializing Module Functionality
    self.initRoutes();
    self.initModel();

    return this;
  },

  /**
   * Log message
   *
   * @param {String} message
   * @param {String} category
   * @return {Logger}
   */
  log: function(message, category) {
    var self = this;

    var log = new self.mongoose.models.Log();
    log.message = message;
    if(category) {
      log.category = category;
    }
    log.save();

    if (self.options.console) {
      console.log(message);
    }
    return this;
  }
}


var logger = module.exports = exports = new Logger;
